# Simple React Native App
===========================================

## Requirements

1. NodeJS LTS (Long Term Support) version 10.15.3
2. Python 2.7
3. Android Studio for Android OR XCode for IOS
4. Android SDK environtment variable setttings => ANDROID_HOME
5. Emulator

## Getting Started

1. Clone this repo, `git clone https://gitlab.com/Arisma/react_native_sample.git <your project name>`
2. Go to project's root directory, `cd <your project name>`
3. Run `npm install` to install dependencies
4. Run `npm install -g react-native-cli` to install command line interface for react-native

## How to Run

1. Start the packager with `npm start`
1. Debugging with `npm run android` to run android project OR `npm run ios` (need Macbook to execute run ios)
2. Connect a mobile device to your development machine OR run emulator 

## Possible Error
1. liscense for android SDK. (copy all file in the 'licenses' dir of your sdk to the root dir of sdk)
2. No emulator/device connected. Please connect your device to the PC/laptop. In my case, Memu Player doesn't detected as emulator.
	
Enjoy Code!!!