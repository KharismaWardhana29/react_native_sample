import React, { Component } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native';

export default class navbar extends Component {
  render() {
    return (
      <View style={styles.container}>
        
        <View>
          <Image style={styles.logo} source={require('../assets/images/logo.png')}/>
        </View>

        <View>
            <Text style={styles.textNavbar}> Waroeng Kita </Text>
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{   
    backgroundColor:'#3684AE',
    flexDirection:'row',
    alignContent:'flex-end',
    justifyContent:'center',
    height:60,
    width:'100%'
  },

  logo:{
    width:55,
    height:55,
  },

  textNavbar:{  
    fontSize: 23,
    fontWeight: 'bold',
    fontFamily:'Roboto',
    textAlign:'center',
    color:'#FFFFFF',
    marginTop:12
  },

})