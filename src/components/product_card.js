import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, Alert, TouchableOpacity } from 'react-native';
import { ProductConsumer } from "./../helper";

export default class product_card extends Component {
  constructor(props) {
    super(props)
    this.state = { count: 0 }
  }

  onPress = () => {
    // Alert.alert(
    //   'Title',
    //   'Msg',
    //   [
    //     {
    //       text: 'Cancel',
    //       onPress: () => console.log('Cancel Pressed'),
    //       style: 'cancel',
    //     },
    //     {text: 'OK', onPress: () => console.log('OK Pressed')},
    //   ],
    //   {cancelable: false},
    // );
    this.props.navigation.navigate('DetailsProduct',{product: this.props.product});
  }

  render() {
    const { id, title, img, price, inCart } = this.props.product;
    return (
        <View style={styles.card}>
          <TouchableOpacity onPress={this.onPress} activeOpacity={0.7}>
            <ProductConsumer>
                {value => {
                  return (
                    <View>
                      <Image style={styles.img} source={ img } />
                    </View>
                  );
                }}
              </ProductConsumer>
              <Text style={styles.titletag}>{title}</Text>
          </TouchableOpacity >
        </View>
    )
  }
}

const styles = StyleSheet.create({
  card:{
    marginTop:30,
    marginBottom:20,
    width: '100%',
    alignItems:'center',
    justifyContent:'center',
    height: '10%'
  },

  titletag:{
    alignItems:'center',
    justifyContent:'center',
    textAlign:'center',
    color:'#fff',
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor:'#3684AE',
  },

  pricetag:{
    textAlign:'center',
    justifyContent:'center',
    color:'#FB3374',
    fontSize: 14,
    fontWeight: 'bold',
  },

  img:{  
    width: 250,
    height: 220 
  },

})