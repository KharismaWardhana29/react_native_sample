import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native';

import { storeProducts } from './../../data';
import { ProductConsumer } from './../../helper';

import Navbar from '../../components/navbar';
import ProductCard from '../../components/product_card';

export default class Home extends Component {
   static navigationOptions = {
    headerTitle: <Navbar/>
  };

  constructor (){
    super()
    this.state = {
      products: storeProducts
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textListMenu}> Menu Kita </Text>
 
        <ScrollView style={styles.wrapperList}>
              <ProductConsumer>
                  {value => {
                    return this.state.products.map(product => {
                      return <ProductCard key={product.id} product={product} navigation={this.props.navigation}/>;
                    });
                  }}
              </ProductConsumer>
        </ScrollView>
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  
  textListMenu:{
    marginTop:5,
    marginBottom:5,
    fontSize:25,
    fontWeight:'bold',
    color:'#3684AE',
    textAlign:'center'
  },

  container:{
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },

  wrapperList:{
    margin:10,
  }

})