import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, Dimensions, Button } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default class Product extends Component {
  
  static navigationOptions = {
    title: 'Details',
    headerStyle: {
      backgroundColor: '#3684AE',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  render() {
    const { navigation } = this.props;
    const product = navigation.getParam('product');
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{ product.title }</Text>
        <Image style={styles.image} source={product.img} />
        <Text style={styles.priceTag}>Price ${ product.price }</Text>
        <Button title={'Buy'}></Button>
        <ScrollView>
          <Text style={styles.desc}>{ product.info }</Text>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{   
    marginLeft: 10,
    marginRight: 10,
    height: Dimensions.get('window').height
  },

  title:{   
    fontSize:18,
    fontWeight:'bold',
    textAlign:'center'
  },

  image:{
    width:'100%',
    height: 250,
    borderColor:'#00CABA',
    borderWidth:5,
    borderRadius:5,
    marginTop:5,
    alignContent:'center',
    justifyContent:'center'
  },

  priceTag:{   
    fontSize:18,
    fontWeight:'bold',
    color:'#FB3374',
  },

  desc:{
    width:'100%',
    height:'100%',
  }
})