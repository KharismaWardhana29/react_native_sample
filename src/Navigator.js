import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Home from './screens/Home/Home';
import Details from './screens/Product/Product';
import Login from './screens/Login/Login';


const AppStack = createStackNavigator(
    {
      Homescreen: Home,
      DetailsProduct: Details,
    }
  );

const AppContainer = createAppContainer(AppStack);

export default AppContainer;